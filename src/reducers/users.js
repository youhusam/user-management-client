import { actions } from '../config/constants'
const {
  USERS: {
    GET_USERS_REQUEST,
    GET_USERS_SUCCESS,
    GET_USERS_FAILURE
  }
} = actions

function users(state = {
  isFetching: false,
  users: []
}, action) {
  switch (action.type) {
    case GET_USERS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      })
    case GET_USERS_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        users: action.response
      })
    case GET_USERS_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.response
      })
    default:
      return state
  }
}

export default users
