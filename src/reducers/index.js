import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import reduceReducers from 'reduce-reducers'
import auth from './auth'
import currentUser from './currentUser'
import users from './users'

const combinedReducers = combineReducers({
  routing: routerReducer,
  auth,
  currentUser,
  users
})

export default reduceReducers(combinedReducers, currentUser)
