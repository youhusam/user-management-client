import { actions } from '../config/constants'
const {
  CURRENT_USER: {
    UPDATE_STATUS_SUCCESS,
    UPDATE_STATUS_REQUEST,
    UPDATE_STATUS_FAILURE
  }
} = actions

function currentUser(state = {
  currentUser: {
    isFetching: false
  }
}, action) {
  switch (action.type) {
    case UPDATE_STATUS_REQUEST:
      return Object.assign({}, state, {
        currentUser: {
          isFetching: true
        },
      })
    case UPDATE_STATUS_SUCCESS:
      localStorage.setItem('user', action.response)
      return Object.assign({}, state, {
        currentUser: {
          errorMessage: '',
          isFetching: false
        },
        auth: {
          ...state.auth,
          user: action.response
        }
      })
    case UPDATE_STATUS_FAILURE:
      return Object.assign({}, state, {
        currentUser: {
          isFetching: false,
          errorMessage: action.response
        },
      })
    default:
      return state
  }
}

export default currentUser
