import { CALL_API } from '../middleware/api'

import { actions } from '../config/constants'
const {
  CURRENT_USER: {
    UPDATE_STATUS_FAILURE,
    UPDATE_STATUS_SUCCESS,
    UPDATE_STATUS_REQUEST
  }
} = actions

export function updateStatus(status) {
  return {
    [CALL_API]: {
      endpoint: 'me',
      authenticated: true,
      method: 'PATCH',
      body: { status },
      types: [UPDATE_STATUS_REQUEST, UPDATE_STATUS_SUCCESS, UPDATE_STATUS_FAILURE]
    }
  }
}
