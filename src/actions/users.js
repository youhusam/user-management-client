import { CALL_API } from '../middleware/api'

import { actions } from '../config/constants'
const {
  USERS: {
    GET_USERS_REQUEST,
    GET_USERS_SUCCESS,
    GET_USERS_FAILURE
  }
} = actions

export function getUsers(filters) {

  return {
    [CALL_API]: {
      endpoint: 'users',
      authenticated: true,
      body: filters,
      types: [GET_USERS_REQUEST, GET_USERS_SUCCESS, GET_USERS_FAILURE]
    }
  }
}
