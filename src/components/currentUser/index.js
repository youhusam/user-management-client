import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  updateStatus
} from '../../actions/currentUser'
import { userStatus } from '../../config/constants'
import classes from './styles.scss'

class CurrentUser extends Component {

  constructor(props) {
    super(props)
    this.state = {
      selectedStatus: props.user.status
    }

    this.handleStatusChange = this.handleStatusChange.bind(this)
  }

  handleStatusChange(event) {
    const status = event.target.value
    this.setState({ selectedStatus: status })
    this.props.updateStatus(status)
  }

  renderDropDown() {
    const currentUserStatus = this.state.selectedStatus
    const options = []

    for (const constantName in userStatus) {
      const value = userStatus[constantName]
      options.push(<option key={constantName} value={value}>{value}</option>)
    }

    return (
      <select
        value={currentUserStatus}
        onChange={this.handleStatusChange}
        className={classes.dropDown}
      >
        {options}
      </select>
    )
  }

  render() {
    const {
      user
    } = this.props

    return (
      <div className={classes.currentUser}>
        <p className={classes.greeting}>
          Hello {user.username}! You are&nbsp;
          <span className={classes.status}>{user.status}</span>.
        </p>
        <div>
          <div className={classes.updateStatusDescription}>
            Update my current status:
          </div>
          {this.renderDropDown()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
})

const mapDispatchToProps = dispatch => bindActionCreators({
  updateStatus
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentUser)
