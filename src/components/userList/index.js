import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  getUsers
} from '../../actions/users'
import { userStatus } from '../../config/constants'
import classes from './styles.scss'

class UserList extends Component {
  debounceTimeout = null

  constructor(props) {
    super(props)
    this.state = {
      status: '0',
      searchValue: ''
    }

    this.handleStatusChange = this.handleStatusChange.bind(this)
    this.handleSearchChange = this.handleSearchChange.bind(this)
    this.debounce = this.debounce.bind(this)
    this.getUsers = this.getUsers.bind(this)
  }

  componentDidMount() {
    this.getUsers()
  }

  debounce(cb) {
    if (this.debounceTimeout) {
      clearTimeout(this.debounceTimeout)
      this.debounceTimeout = null
    }
    this.debounceTimeout = setTimeout(() => {
      this.debounceTimeout = null
      cb()
    }, 500)
  }

  getUsers() {
    const filters = {
      username: this.state.searchValue
    }
    if (this.state.status !== '0') {
      filters.status = this.state.status
    }
    this.props.getUsers(filters)
  }

  handleStatusChange(event) {
    const status = event.target.value
    this.setState({ status }, this.getUsers)
  }

  handleSearchChange(event) {
    const searchValue = event.target.value
    this.setState({ searchValue }, () => this.debounce(() => this.getUsers()))
  }

  renderDropDown() {
    const options = [
      <option key="0" value="0">Filter By Status...</option>
    ]

    for (const constantName in userStatus) {
      options.push(<option key={constantName}>{userStatus[constantName]}</option>)
    }

    return (
      <select
        value={this.state.status}
        onChange={this.handleStatusChange}
        className={classes.dropdown}
      >
        {options}
      </select>
    )
  }

  renderSearch() {
    return (
      <input
        type="text"
        onChange={this.handleSearchChange}
        placeholder="Search By Name..."
        className={classes.search}
      />
    )
  }

  renderUserList() {
    const { isFetching, users } = this.props.users

    if (isFetching) return

    if (!users.length)  return (
      <div>
        No users found with the current filters.
      </div>
    )

    return (
      <div className={classes.userList}>
        {
          users.map(user =>
            <div
              key={user.id}
              className={`${classes.user} ${user.status == userStatus.ON_VACATION && classes.onVacation}`}
            >
              {user.username} ({user.status})
            </div>
          )
        }
      </div>
    )
  }

  render() {
    return (
      <div>
        <div className={classes.description}>
          Employees:
        </div>
        <div className={classes.filters}>
          {this.renderSearch()}
          {this.renderDropDown()}
        </div>
        {this.renderUserList()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  users: state.users
})

const mapDispatchToProps = dispatch => bindActionCreators({
  getUsers
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList)
