import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { logoutUser } from '../../actions/auth'
import classes from './styles.scss'

class Header extends Component {
  static propTypes = {
    username: PropTypes.string.isRequired,
    dispatch: PropTypes.func.isRequired
  }

  render() {
    const { dispatch, username } = this.props;
    return (
      <header className={classes.header}>
        <div className={classes.headerInner}>
          <p className={classes.title} >MyWorkStatus</p>

          <div>
            <span className={classes.username} >{username}</span>
            <span className={classes.logout} onClick={() => { dispatch(logoutUser()) }}>Logout</span>
          </div>
        </div>
      </header>
    )
  }
}

export default Header
