import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Home from '../home'
import Header from '../../components/header'
import PropTypes from 'prop-types'
import AuthRoute from '../../components/authRoute'
import Login from '../login'
import classes from './styles.scss'

class App extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    username: PropTypes.string.isRequired
  }

  render() {
    const { dispatch, isAuthenticated, username } = this.props

    return (
      <div>
        {isAuthenticated && <Header username={username} dispatch={dispatch} />}

        <main className={isAuthenticated ? classes.main : null}>
          <AuthRoute exact path="/" component={Home} authed={isAuthenticated} />
          <Route exact path="/login" component={Login} />
        </main>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { auth, auth: { isAuthenticated } } = state
  const username = auth.user ? auth.user.username : '';

  return {
    isAuthenticated,
    username
  }
}

export default withRouter(connect(mapStateToProps)(App))
