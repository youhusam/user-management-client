import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { loginUser } from '../../actions/auth'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Redirect } from 'react-router-dom'
import classes from './styles.scss'

class Login extends Component {
  static propTypes = {
    loginUser: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      username: ''
    }
    this.handleLogin = this.handleLogin.bind(this)
  }

  handleLogin(e) {
    e.preventDefault()
    if (this.state.username.length)
      this.props.loginUser({ username: this.state.username })
  }

  render() {
    const { isAuthenticated } = this.props
    if (isAuthenticated)
      return (
        <Redirect to={{
          pathname: '/'
        }} />
      )

    return (
      <div className={classes.loginContainer}>
        <div className={classes.loginInner}>
          <p>Welcome to MyWorkStatus</p>
          <form onSubmit={this.handleLogin}>
            <input type="text" onChange={e => this.setState({ username: e.target.value })} placeholder="Username" className={classes.username} />
            <input type="submit" value="Login" className={classes.login} disabled={!this.state.username.length} />
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  loginUser
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)
