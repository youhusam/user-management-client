import React, { Component } from 'react'
import CurrentUser from '../../components/currentUser'
import UserList from '../../components/userList'
import classes from './styles.scss'

class Home extends Component {

  render() {
    return (
      <div className={classes.home}>
        <div className={classes.homeInner}>
          <CurrentUser />
          <UserList />
        </div>
      </div>
    )
  }
}

export default Home
