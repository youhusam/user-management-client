const BASE_URL = 'http://localhost:8000/api/'

function callApi(endpoint, authenticated, method, body) {

  let token = localStorage.getItem('token') || null
  let config = {}
  let query = ''
  let requestBody = null
  if ((!method || method === 'GET') && body) {
    requestBody = null
    query = '?'
    for (const q in body) {
      query += `${encodeURIComponent(q)}=${encodeURIComponent(body[q])}&`
    }
  } else {
    requestBody = body && JSON.stringify(body)
  }

  if (authenticated) {
    if (token) {
      config = {
        method: method || 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        body: requestBody
      }
    } else {
      throw new Error("No token saved!")
    }
  }

  return fetch(BASE_URL + endpoint + query, config)
    .then(response =>
      response.json()
        .then(json => ({ json, response }))
    ).then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json)
      }

      return json
    }).catch(err => console.log(err))
}

export const CALL_API = Symbol('Call API')

export default store => next => action => {
  const callAPI = action[CALL_API]

  // So the middleware doesn't get applied to every single action
  if (typeof callAPI === 'undefined') {
    return next(action)
  }

  let { endpoint, types, authenticated, method, body } = callAPI

  const [ requestType, successType, errorType] = types

  next({
    type: requestType
  })

  return callApi(endpoint, authenticated, method, body).then(
    response =>
      next({
        response,
        authenticated,
        type: successType
      }),
    error => next({
      error: error.message || 'There was an error.',
      type: errorType
    })
  )
}
